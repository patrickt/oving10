// @flow
/* eslint eqeqeq: "off" */

import ReactDOM from "react-dom";
import * as React from "react";
import { Component } from "react-simplified";
import { HashRouter, Route, NavLink } from "react-router-dom";
import { Alert } from "./widgets";
import "bootstrap/dist/css/bootstrap.min.css";

import createHashHistory from "history/createHashHistory";
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

class Student {
  id: number;
  static nextId = 1;

  firstName: string;
  lastName: string;
  email: string;

  constructor(firstName: string, lastName: string, email: string) {
    this.id = Student.nextId++;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
let students = [
  new Student("Ola", "Jensen", "ola.jensen@ntnu.no"),
  new Student("Kari", "Larsen", "kari.larsen@ntnu.no")
];

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

function findMonth(month: number) {
  if (month > 12) {
    return null;
  } else if (month < 1) {
    return null;
  } else if (month == 1) {
    return "Jan";
  } else if (month == 10) {
    return "Oct";
  }
}

class FindDate {
  day: number;
  month;

  constructor() {
    var today = new Date();
    this.day = today.getDate();
    this.month = today.getMonth() + 1;
    if (this.month == 1) {
      this.month = "Jan";
    } else if (this.month == 10) {
      this.month = "Okt";
    } else if (this.month == 2) {
      this.month = "Feb";
    } else if (this.month == 3) {
      this.month = "Mar";
    } else if (this.month == 4) {
      this.month = "Apr";
    } else if (this.month == 5) {
      this.month = "May";
    } else if (this.month == 6) {
      this.month = "Jun";
    } else if (this.month == 7) {
      this.month = "Jul";
    } else if (this.month == 8) {
      this.month = "Aug";
    } else if (this.month == 9) {
      this.month = "Sep";
    } else if (this.month == 11) {
      this.month = "Nov";
    } else {
      this.month = "Des";
    }
  }
}

class NewsCase {
  id: number;
  static nextId = 1;

  title: string;
  date: string;
  category: string;
  pictureUrl: string;
  text: string;

  constructor(
    title: string,
    date: string,
    category: string,
    pictureUrl: string,
    text: string
  ) {
    this.title = title;
    this.date = date;
    this.category = category;
    this.pictureUrl = pictureUrl;
    this.text = text;
  }
}

let cases = [
  new NewsCase(
    "Oilers forbi Vålerenga på tabellen",
    "Oct 21",
    "Sport",
    "https://smp.vgc.no/v2/images/313edf09-be76-4254-9320-cb406477d843?fit=crop&h=1267&w=1900&s=55488cd2bf0654c01d34e940180d9009a60f6c48",
    "(Vålerenga-Stavanger Oilers 1–3) Med en sterk borteseier i hovedstaden tok Stavanger Oilers seg forbi Vålerenga på tabellen."
  ),
  new NewsCase(
    "PUBG beklager servertrøbbel og gir spillere en lue som plaster på såret",
    "Oct 22",
    "Dataspill",
    "https://o.aolcdn.com/images/dims?quality=100&image_uri=http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F8db17a3120d671ee6a2427ea1d8c53ec%2F206345127%2Fpubg-ed.jpg&client=amp-blogside-v2&signature=edeb4e9fba9137e8c59df7fa4a4ea85badd42c74",
    "Utvikleren har også sluppet detaljer om spillets neste oppdatering."
  ),
  new NewsCase(
    "Dette er verdens beste turistby",
    "Oct 10",
    "Kultur",
    "https://vgc.no/drfront/images/2018/10/23/c=139,51,1162,1162;w=318;h=318;430949.jpg",
    "Det er ikke bare deilig å være norsk i Danmark. Alle kan føle seg vel i København. Så vel at byen nå troner på toppen av Lonely Planets prestisjetunge ti-på-topp-liste for byer i 2019."
  )
];

class NewsList extends Component {
  render() {
    return (
      <div class="row mb-2">
        {cases.map(NewsCase => (
          <div class="col-md-6" key={NewsCase.id}>
            <div class="card flex-md-row mb-4 shadow-sm h-md-250">
              <div class="card-body d-flex flex-column align-items-start">
                <strong class="d-inline-block mb-2 text-success">
                  {NewsCase.category}
                </strong>
                <h3 class="mb-0">
                  <a class="text-dark" href="#">
                    {NewsCase.title}
                  </a>
                </h3>
                <div class="mb-1 text-muted">{NewsCase.date}</div>
                <p class="card-text mb-auto">{NewsCase.text}</p>
                <a href="#">Les videre</a>
              </div>
              <img
                class="card-img-right flex-auto d-none d-lg-block"
                src={NewsCase.pictureUrl}
                alt="Card image cap"
                height="300px"
              />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

class NewCase extends Component {
  title = "";
  date = "";
  category = "";
  pictureUrl = "";
  text = "";

  render() {
    return (
      <div class="mx-auto" style={{ width: "1000px" }}>
        <form>
          <div class="form-group">
            <label for="inpTitle">Tittel</label>
            <input
              type="title"
              value={this.title}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.title = event.target.value)
              }
              class="form-control"
              id="inpTitle"
              aria-describedby="titleHelp"
              placeholder="Tittel på innlegget"
            />
            <small id="titleHelp" class="form-text text-muted">
              Tittlen bør vekke interesse for innlegget ditt!
            </small>
          </div>
          <div class="form-group">
            <label for="inpText">Forsidetekst</label>
            <input
              type="text"
              value={this.text}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.text = event.target.value)
              }
              class="form-control"
              id="inpText"
              placeholder="Forsidetekst"
            />
            <small id="textHelp" class="form-text text-muted">
              Denne teksten vil vises på forsiden sammen med tittlen og bildet
              ditt
            </small>
          </div>
          <div class="form-group">
            <label for="inpCategory">
              Kategori som passer til innlegget ditt
            </label>
            <select
              class="form-control"
              id="inpCategory"
              value={this.category}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.category = event.target.value)
              }
            >
              <option>Nyheter</option>
              <option>Sport</option>
              <option>Kultur</option>
              <option>Dataspill</option>
              <option>Politikk</option>
            </select>
          </div>
          <div class="form-group">
            <label for="inpInnleggstekst">Innleggstekst</label>
            <textarea class="form-control" id="exampleTextarea" rows="5" />
          </div>
          <fieldset class="form-group">
            <legend>Viktighet</legend>
            <div class="form-check">
              <label class="form-check-label">
                <input
                  type="radio"
                  class="form-check-input"
                  name="optionsRadios"
                  id="optionsRadios1"
                  value="1"
                  checked
                />
                Veldig viktig - hvis du velger dette vil saken din vises på
                forsiden
              </label>
            </div>
            <div class="form-check">
              <label class="form-check-label">
                <input
                  type="radio"
                  class="form-check-input"
                  name="optionsRadios"
                  id="optionsRadios2"
                  value="2"
                />
                Mindre viktig - hvis du velger dette vil saken din kun vises
                under kategorien du har valgt
              </label>
            </div>
          </fieldset>
          <div class="form-group">
            <label for="inpUrl">Url til bilde</label>
            <input
              type="text"
              value={this.pictureUrl}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.pictureUrl = event.target.value)
              }
              class="form-control"
              id="inpUrl"
              placeholder="https://"
            />
          </div>
          <button type="submit" class="btn btn-primary" onClick={this.save}>
            Post
          </button>
        </form>
      </div>
    );
  }

  save() {
    var day = new FindDate();
    cases.push(
      new NewsCase(
        this.title,
        day.month + " " + day.day,
        this.category,
        this.pictureUrl,
        this.text
      )
    );

    history.push("/");
  }
}

class Menu extends Component {
  render() {
    return (
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <NavLink class="navbar-brand" exact to="/">
          <img
            src="https://fanart.tv/fanart/movies/8388/hdmovielogo/three-amigos-5871a7ecacb18.png"
            height="74"
            width="200"
            alt="Gå til forsiden"
          />
        </NavLink>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/nyheter"
                style={{ margin: "20px" }}
              >
                Nyheter
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/sport"
                style={{ margin: "20px" }}
              >
                Sport
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink
                class="nav-link"
                exact
                to="/kultur"
                style={{ margin: "20px" }}
              >
                Kultur
              </NavLink>
            </li>
            <li class="nav-item">
              <Dropdown />
            </li>
          </ul>
        </div>
        <div
          class="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul class="navbar-nav">
            <li class="nav-item">
              <NavLink class="nav-link" exact to="/nysak">
                Skriv ny sak
              </NavLink>
            </li>
            <li class="nav-item">
              <NavLink class="nav-link" exact to="/login">
                Logg inn
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

class Home extends Component {
  render() {
    return (
      <div class="row mb-2">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-success">Nyheter</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">
                  Erna og Obama POG
                </a>
              </h3>
              <div class="mb-1 text-muted">Nov 11</div>
              <p class="card-text mb-auto">
                This is a wider card with supporting text below as a natural
                lead-in to additional content.
              </p>
              <a href="#">Les videre</a>
            </div>
            <img
              class="card-img-right flex-auto d-none d-lg-block"
              src="https://imbo.vgc.no/users/e24/images/67d7e92e93cad2001d9068302ea55253?t%5B0%5D=crop%3Awidth%3D1985%2Cheight%3D1116%2Cx%3D736%2Cy%3D111&t%5B1%5D=maxSize%3Awidth%3D990&t%5B2%5D=resize%3Awidth%3D990&accessToken=e8bac780354e3105ae6d162610b6eb6cc5cd65f39b8a1d5bf3684f217faeec30"
              alt="Card image cap"
              height="300px"
            />
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-success">Nyheter</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">
                  Erna og Obama POG
                </a>
              </h3>
              <div class="mb-1 text-muted">Nov 11</div>
              <p class="card-text mb-auto">
                This is a wider card with supporting text below as a natural
                lead-in to additional content.
              </p>
              <a href="#">Les videre</a>
            </div>
            <img
              class="card-img-right flex-auto d-none d-lg-block"
              src="https://imbo.vgc.no/users/e24/images/67d7e92e93cad2001d9068302ea55253?t%5B0%5D=crop%3Awidth%3D1985%2Cheight%3D1116%2Cx%3D736%2Cy%3D111&t%5B1%5D=maxSize%3Awidth%3D990&t%5B2%5D=resize%3Awidth%3D990&accessToken=e8bac780354e3105ae6d162610b6eb6cc5cd65f39b8a1d5bf3684f217faeec30"
              alt="Card image cap"
              height="300px"
            />
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 shadow-sm h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-success">Nyheter</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">
                  Erna og Obama POG
                </a>
              </h3>
              <div class="mb-1 text-muted">Nov 11</div>
              <p class="card-text mb-auto">
                This is a wider card with supporting text below as a natural
                lead-in to additional content.
              </p>
              <a href="#">Les videre</a>
            </div>
            <img
              class="card-img-right flex-auto d-none d-lg-block"
              src="https://imbo.vgc.no/users/e24/images/67d7e92e93cad2001d9068302ea55253?t%5B0%5D=crop%3Awidth%3D1985%2Cheight%3D1116%2Cx%3D736%2Cy%3D111&t%5B1%5D=maxSize%3Awidth%3D990&t%5B2%5D=resize%3Awidth%3D990&accessToken=e8bac780354e3105ae6d162610b6eb6cc5cd65f39b8a1d5bf3684f217faeec30"
              alt="Card image cap"
              height="300px"
            />
          </div>
        </div>
      </div>
    );
  }
}

class StudentList extends Component {
  render() {
    return (
      <ul>
        {students.map(student => (
          <li key={student.email}>
            <NavLink
              activeStyle={{ color: "darkblue" }}
              exact
              to={"/students/" + student.id}
            >
              {student.firstName} {student.lastName}
            </NavLink>{" "}
            <NavLink
              activeStyle={{ color: "darkblue" }}
              to={"/students/" + student.id + "/edit"}
            >
              edit
            </NavLink>
          </li>
        ))}
      </ul>
    );
  }
}

class StudentDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    let student = students.find(
      student => student.id == this.props.match.params.id
    );
    if (!student) {
      Alert.danger("Student not found: " + this.props.match.params.id);
      return null; // Return empty object (nothing to render)
    }
    return (
      <div>
        <ul>
          <li>First name: {student.firstName}</li>
          <li>Last name: {student.lastName}</li>
          <li>Email: {student.email}</li>
        </ul>
      </div>
    );
  }
}

class Dropdown extends React.Component {
  state = {
    isOpen: false
  };

  toggleOpen = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    const menuClass = `dropdown-menu${this.state.isOpen ? " show" : ""}`;
    return (
      <HashRouter>
        <div
          className="dropdown"
          onClick={this.toggleOpen}
          style={{ margin: "20px" }}
        >
          <button
            className="btn btn-secondary dropdown-toggle"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
          >
            Flere kategorier
          </button>
          <div className={menuClass} aria-labelledby="dropdownMenuButton">
            <NavLink className="dropdown-item" to="/nyheter">
              Dataspill
            </NavLink>
            <NavLink className="dropdown-item" to="/sport">
              Politikk
            </NavLink>
            <NavLink className="dropdown-item" to="/kultur">
              Dank memes
            </NavLink>
          </div>
        </div>
      </HashRouter>
    );
  }
}

ReactDOM.render(<Dropdown />, document.getElementById("root"));

class StudentEdit extends Component<{ match: { params: { id: number } } }> {
  firstName = ""; // Always initialize component member variables
  lastName = "";
  email = "";

  render() {
    return (
      <form>
        <ul>
          <li>
            First name:{" "}
            <input
              type="text"
              value={this.firstName}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.firstName = event.target.value)
              }
            />
          </li>
          <li>
            Last name:{" "}
            <input
              type="text"
              value={this.lastName}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.lastName = event.target.value)
              }
            />
          </li>
          <li>
            Email:{" "}
            <input
              type="text"
              value={this.email}
              onChange={(event: SyntheticInputEvent<HTMLInputElement>) =>
                (this.email = event.target.value)
              }
            />
          </li>
        </ul>
        <button type="button" onClick={this.save}>
          Save
        </button>
      </form>
    );
  }

  // Initialize component state (firstName, lastName, email) when the component has been inserted into the DOM (mounted)
  mounted() {
    let student = students.find(
      student => student.id == this.props.match.params.id
    );
    if (!student) {
      Alert.danger("Student not found: " + this.props.match.params.id);
      return;
    }

    this.firstName = student.firstName;
    this.lastName = student.lastName;
    this.email = student.email;
  }

  save() {
    let student = students.find(
      student => student.id == this.props.match.params.id
    );
    if (!student) {
      Alert.danger("Student not found: " + this.props.match.params.id);
      return;
    }

    student.firstName = this.firstName;
    student.lastName = this.lastName;
    student.email = this.email;

    // Go to StudentDetails after successful save
    history.push("/students/" + student.id);
  }
}

function studentsLoad() {
  history.push("/students/");
}

const root = document.getElementById("root");
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={NewsList} />
        <Route path="/nysak" component={NewCase} />
        <Route exact path="/students/:id" component={StudentDetails} />
        <Route exact path="/students/:id/edit" component={StudentEdit} />
      </div>
    </HashRouter>,
    root
  );
